Some simple UI components for Vue.js 2.0+ projects.

Running:

Install dependencies
`npm install`

Serve with hot reload at localhost:8080
`npm run dev`
